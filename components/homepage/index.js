import React from 'react';
import { classPrefixor } from 'utils/classPrefixor';
import useChangeMeta from 'components/common/hook/useChangeMeta.js';
import image from '../../assets/images/background.jpg';
import Link from 'next/link';
import { images } from 'utils/images';

// Hook

const prefix = 'homepage';
const c = classPrefixor(prefix);

const HomePage = () => {
  useChangeMeta('Cho thuê xe du lịch - xe sang vip pro luxury');
  const style = {
    backgroundImage: `url(${image})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat'
  };
  return (
    <div className={c`wrapper-page`}>
      <section className="content-place" style={style}>
        <Link href="/find">
          <div className="header-content">
            <h1>Tìm xe ngay!</h1>
            <h1>Tìm xe ngay!</h1>
          </div>
        </Link>
        <div className="sky">
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Sedan-car.svg/1280px-Sedan-car.svg.png" />
          <div className="rode1"></div>
          <div className="rode2"></div>
        </div>
      </section>
      <div className="main-content">
        <div className="slide-render">
          <h3 className="title-car">Tính năng nổi bật</h3>
          <div className="carousel-content">
            <img src={images.carousel_1} />
            <img src={images.carousel_2} />
            <img src={images.carousel_3} />
            <img src={images.carousel_4} />
          </div>
        </div>
      </div>
    </div>
  );
};
export default HomePage;
