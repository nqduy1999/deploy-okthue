import { Modal, Button, Form, Input } from 'antd';
import { AccountContext } from 'components/common/context/AccountContext';
import React, { useContext } from 'react';
import { classPrefixor } from 'utils/classPrefixor';
import {
  CloseCircleOutlined,
  LeftCircleOutlined,
  MailOutlined
} from '@ant-design/icons';
import { useState } from 'react';
import * as Validator from 'utils/validatorFormat';
import { useDispatch } from 'react-redux';
import { SignInAction } from 'action/AuthAction';

// Hook

const prefix = 'sign-up';
const c = classPrefixor(prefix);

const LoginWithPhone = () => {
  const { modal, setModal } = useContext(AccountContext);
  const [state, setState] = useState({
    stateClick: false,
    email: ''
  });
  const { stateClick, email } = state;
  const dispatch = useDispatch();
  const Login = value => {
    const data = {
      email: email,
      password: value.password
    };
    if (value.password) {
      dispatch(SignInAction(data));
    }
  };
  const [form] = Form.useForm();
  return (
    <Modal
      className={c`modal`}
      visible={modal}
      footer={null}
      title={null}
      centered={true}
      closeIcon={<CloseCircleOutlined />}
      onCancel={() => setModal(false)}
    >
      {stateClick && (
        <LeftCircleOutlined
          className="back-to-email"
          onClick={() => setState({ ...state, stateClick: false })}
        />
      )}
      <Form className="body" name="input-phone" form={form} onFinish={Login}>
        <i className="fa fa-fire"></i>
        {stateClick ? <h1>Đăng Nhập </h1> : ''}
        <div className="body__title-content">
          {!stateClick ? (
            <>
              <h1>Nhập email của bạn </h1>
              <Form.Item
                name="email"
                className="input-form-item"
                rules={[Validator.required('Mail', 'Không được bỏ trống')]}
              >
                <Input
                  prefix={
                    <>
                      <MailOutlined />
                    </>
                  }
                  onChange={e => setState({ ...state, email: e.target.value })}
                />
              </Form.Item>
              <div
                className="body__title-content__main-phone"
                data-nosnippet="true"
              >
                Khi bạn nhấn Tiếp tục , OkThuê sẽ gửi cho bạn một tin nhắn có
                chứa xác thực. Bạn có thể phải trả phí tin nhắn và dữ liệu. Số
                điện thoại thoại được xác thực dùng để đăng nhập.
              </div>
            </>
          ) : (
            <>
              <div>Email: {email}</div>
              <Form.Item name="password" className="input-form-item">
                <Input.Password placeholder="Nhập mật khẩu" />
              </Form.Item>
            </>
          )}
        </div>
        <div className="btn-action">
          {!stateClick ? (
            <Button
              type="default"
              className="button-login continue-button"
              onClick={() => setState({ ...state, stateClick: true })}
            >
              Tiếp tục
            </Button>
          ) : (
            <Button
              type="default"
              className="button-login continue-button"
              htmlType="submit"
            >
              Đăng nhập
            </Button>
          )}
          {!stateClick ? (
            <Button
              type="primary"
              className="button-login"
              onClick={() => setState({ ...state, stateClick: true })}
            >
              Đăng Ký
            </Button>
          ) : (
            ''
          )}
        </div>
      </Form>
    </Modal>
  );
};
export default LoginWithPhone;
