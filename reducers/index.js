import { combineReducers } from 'redux';
import accountReducer from './account.reducer';

export default combineReducers({
  accountReducer
});
