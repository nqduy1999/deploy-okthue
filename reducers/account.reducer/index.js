import { AUTHENTICATION_TYPE } from 'constant';

const initialState = {
  error: null,
  isLoading: false,
  isAuthenticated: false,
  auth_token: null,
  message: null,
  data: null,
  status: null
};

const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATION_TYPE.SIGNIN_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case AUTHENTICATION_TYPE.SIGNIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        // isAuthenticated: true,
        message: null,
        // auth_token: action.payload.auth_token,
        data: action.payload.data
      };
    case AUTHENTICATION_TYPE.SIGNIN_FAILURE:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};

export default accountReducer;
