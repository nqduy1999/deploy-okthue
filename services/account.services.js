import axios from 'utils/axiosServices';
const prefix = 'user/';

class AccountServices {
  SignInService = data => {
    return axios.request({
      method: 'POST',
      url: `${prefix}login`,
      data: data
    });
  };
}
export default AccountServices;
