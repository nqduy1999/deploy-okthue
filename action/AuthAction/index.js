import { AUTHENTICATION_TYPE } from 'constant';
import AccountServices from 'services/account.services';

export const SignInAction = dataDispatch => dispatch => {
  dispatch({
    type: AUTHENTICATION_TYPE.SIGNIN_REQUEST
  });
  return AccountServices.SignInService(dataDispatch)
    .then(res => {
      console.log(res);
      dispatch({
        type: AUTHENTICATION_TYPE.SIGNIN_SUCCESS
      });
      return { error: false, data: res.data };
    })
    .catch(err => {
      dispatch({
        type: AUTHENTICATION_TYPE.SIGNIN_FAILURE
      });
      return { error: true, data: err.response.data.msg };
    });
};
