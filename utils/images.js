import backGround from 'assets/images/background.jpg';
import carousel_1 from 'assets/images/carousel/1.jpg';
import carousel_2 from 'assets/images/carousel/2.jpg';
import carousel_3 from 'assets/images/carousel/3.jpg';
import carousel_4 from 'assets/images/carousel/4.jpg';

//
import icon_appstore_footer from 'assets/images/icon/app-store.png';
import icon_googleplay_footer from 'assets/images/icon/google-play.png';
import icon_google from 'assets/images/icon/google-icon.svg';
import icon_facebook from 'assets/images/icon/facebook.svg';
import icon_chplay from 'assets/images/icon/chplay.svg';
import icon_appstore from 'assets/images/icon/appstore.svg';
const images = {
  backGround: backGround,
  icon_appstore_footer: icon_appstore_footer,
  icon_googleplay_footer: icon_googleplay_footer,
  icon_google: icon_google,
  icon_facebook: icon_facebook,
  icon_chplay: icon_chplay,
  icon_appstore: icon_appstore,
  carousel_4: carousel_4,
  carousel_3: carousel_3,
  carousel_2: carousel_2,
  carousel_1: carousel_1
};

export { images };
